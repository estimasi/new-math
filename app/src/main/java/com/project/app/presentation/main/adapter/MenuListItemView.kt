package com.project.app.presentation.main.adapter

import android.view.View


interface MenuListItemView {
    fun onClickMenu(view: View)
}