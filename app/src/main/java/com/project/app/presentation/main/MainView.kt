package com.project.app.presentation.main

import com.project.framework.core.BaseView
import com.project.framework.widget.LoadingView


interface MainView : BaseView {
    var retryListener: LoadingView.OnRetryListener
}