package com.project.app.presentation.primary

import android.text.TextWatcher
import android.view.View
import com.project.framework.core.BaseView


interface PrimaryView : BaseView {
    var textWatcherA: TextWatcher
    fun onClickCalculate(view: View)
}