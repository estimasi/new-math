package com.project.app.presentation.multiplication

import android.text.TextWatcher
import android.view.View
import com.project.framework.core.BaseView


interface MultiplicationView : BaseView {
    var textWatcherA: TextWatcher
    var textWatcherB: TextWatcher
    fun onClickCalculate(view: View)
}