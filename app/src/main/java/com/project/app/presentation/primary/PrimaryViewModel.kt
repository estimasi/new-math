package com.project.app.presentation.primary

import androidx.databinding.ObservableField
import com.project.framework.core.BaseViewModel


class PrimaryViewModel : BaseViewModel() {
    var bTextA = ObservableField<String>()
    var bTextResult = ObservableField<String>()
    var isShowButton = ObservableField(false)
}