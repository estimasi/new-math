package com.project.app.presentation.addition

import androidx.databinding.ObservableField
import com.project.framework.core.BaseViewModel


class AdditionViewModel : BaseViewModel() {
    var bTextA = ObservableField<String>()
    var bTextB = ObservableField<String>()
    var bTextResult = ObservableField<String>()
    var isShowButton = ObservableField(false)

    var valueA = 0
    var valueB = 0
}