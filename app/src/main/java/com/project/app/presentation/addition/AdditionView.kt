package com.project.app.presentation.addition

import android.text.TextWatcher
import android.view.View
import com.project.framework.core.BaseView


interface AdditionView : BaseView {
    var textWatcherA: TextWatcher
    var textWatcherB: TextWatcher
    fun onClickCalculate(view: View)
}