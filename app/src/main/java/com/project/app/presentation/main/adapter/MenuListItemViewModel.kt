package com.project.app.presentation.main.adapter

import androidx.databinding.ObservableField
import com.project.framework.core.BaseViewModel


class MenuListItemViewModel : BaseViewModel() {
    var bTextTitle = ObservableField<String>()
    var bShowImage = ObservableField(false)
}