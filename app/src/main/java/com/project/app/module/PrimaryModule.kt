package com.project.app.module

import com.project.app.presentation.primary.PrimaryViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val primaryModule = module {

    viewModel { PrimaryViewModel() }
}

