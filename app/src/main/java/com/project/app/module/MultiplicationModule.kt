package com.project.app.module

import com.project.app.presentation.multiplication.MultiplicationViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val multiplicationModule = module {

    viewModel { MultiplicationViewModel() }
}

