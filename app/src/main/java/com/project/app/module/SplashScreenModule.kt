package com.project.app.module

import com.project.app.presentation.splashscreen.SplashScreenViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val splashScreenModule = module {

    viewModel { SplashScreenViewModel() }
}

