package com.project.app.presentation.favorite

import com.project.data.database.AppDatabase
import com.project.data.entities.favorite.Number
import com.project.data.entities.favorite.NumberDAO
import com.project.data.module.databaseModule
import com.project.app.presentation.main.MainActivity
import com.project.data.module.repositoryModule
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.Mock
import org.mockito.MockitoAnnotations


class FavoriteActivityTest : KoinTest {

    @Mock
    lateinit var mainActivity: MainActivity

    /*
     * Inject needed components from Koin
     */
    val appDatabase: AppDatabase by inject()
    val numberDAO: NumberDAO by inject()

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        mainActivity = MainActivity()
        startKoin {
            androidContext(mainActivity)
            modules(listOf(repositoryModule, databaseModule))
        }
    }

    @After
    fun after() {
        appDatabase.close()
    }

    @Test
    fun testSave() {
//        // expected
//        val expectedResult = Number("1", "One")
//
//        // save entities
//        numberDAO.saveNumber(expectedResult)
//
//        // actual
//        val actualResult = numberDAO.findNumberById("1")
//
//        // compare result
//        Assert.assertEquals(expectedResult, actualResult)
    }
}