package com.project.app

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.project.app.presentation.main.MainActivity
import com.project.data.database.AppDatabase
import com.project.data.entities.favorite.Number
import com.project.data.entities.favorite.NumberDAO
import org.junit.*
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class DatabaseTest {
    @get:Rule
    var activityTestRule = ActivityTestRule(MainActivity::class.java)

    private lateinit var dao: NumberDAO
    private lateinit var db: AppDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java)
            .allowMainThreadQueries()
            .build()
        dao = db.numberDAO()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun writeUserAndReadInList() {
        val expectedResult = Number("1", "One")
        // save entities
        dao.saveNumber(expectedResult)

        val actualResult = dao.findNumberById("1")

        Assert.assertEquals(expectedResult, actualResult)
    }
}