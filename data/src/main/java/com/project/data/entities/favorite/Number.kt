package com.project.data.entities.favorite

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "numberFavorite")
data class Number(
//        (autoGenerate = true)
    @PrimaryKey
    val id: String,
    val numberFavorite: String
)