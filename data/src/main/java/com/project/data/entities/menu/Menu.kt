package com.project.data.entities.menu


data class Menu (
    val menu: List<MenuItem>)