package com.project.data.entities.menu


data class MenuItem (
    val id: Int,
    val title: String,
    val imageUrl: String?)