package com.project.data.module

import com.project.data.repository.FavoriteRepository
import com.project.data.repository.FavoriteRepositoryImpl
import com.project.data.repository.MainRepository
import com.project.data.repository.MainRepositoryImpl
import org.koin.dsl.module


val repositoryModule = module {

    single<MainRepository> { MainRepositoryImpl(service = get()) }
    single<FavoriteRepository> { FavoriteRepositoryImpl(dao = get()) }
}