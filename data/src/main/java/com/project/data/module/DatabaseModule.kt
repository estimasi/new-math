package com.project.data.module

import androidx.room.Room
import com.project.data.database.AppDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module


val databaseModule = module {

//    single {
//        Room.databaseBuilder(androidApplication(), AppDatabase::class.java, "app-db")
//            .build()
//    }

    // In-Memory database config for test
    single {
        Room.inMemoryDatabaseBuilder(androidContext(), AppDatabase::class.java)
            .allowMainThreadQueries()
            .build()
    }

    single { get<AppDatabase>().numberDAO() }
}