@file:JvmName("NumberFormattingUtils")

package com.project.framework.extention

import java.math.BigDecimal
import java.text.NumberFormat
import java.util.*


fun Int.formatAsRupiah(): String {
    return if (this < 0) "- Rp " + NumberFormat.getInstance(Locale("in")).format(this * -1)
    else "Rp " + NumberFormat.getInstance(Locale("in")).format(this)
}


fun Long.formatAsRupiah(): String {
    return if (this < 0L) "- Rp " + NumberFormat.getInstance(Locale("in")).format(this * -1)
    else "Rp " + NumberFormat.getInstance(Locale("in")).format(this)
}


fun Double.formatAsRupiah(): String {
    return if (this < 0.0) "- Rp " + NumberFormat.getInstance(Locale("in")).format(this * -1)
    else "Rp " + NumberFormat.getInstance(Locale("in")).format(this)
}


fun BigDecimal.formatAsRupiah(): String {
    return "Rp " + NumberFormat.getInstance(Locale("in")).format(this)
}
